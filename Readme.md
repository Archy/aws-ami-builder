# AWS AMI builder

AWS AMI builder written in Ansible

## Usefull informations

### EC2 startup logs (including user-data execution logs)

* Windows: `C:\ProgramData\Amazon\EC2-Windows\Launch\Log`

## Useful resources

* https://serversforhackers.com/c/an-ansible2-tutorial
* https://docs.ansible.com/ansible/latest/collections/amazon/aws/
* https://docs.ansible.com/ansible/latest/collections/community/aws/

## TODO

[ ] https://aws.amazon.com/blogs/mt/query-for-the-latest-windows-ami-using-systems-manager-parameter-store/
[ ] https://docs.ansible.com/ansible/devel/reference_appendices/config.html#cfg-in-world-writable-dir
[ ] https://docs.ansible.com/ansible/latest/user_guide/windows_performance html#optimise-powershell-performance-to-reduce-ansible-task-overhead
[ ] https://docs.ansible.com/ansible/latest/user_guide/windows_dsc.html
