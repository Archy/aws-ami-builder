$ErrorActionPreference = "Stop"

$PROGRAM_FILES = "C:\Program Files"

$OPENSSH_NAME = "OpenSSH-Win64"
$OPENSSH_ZIP = "$OPENSSH_NAME.zip"
$OPENSSH_DOWNLOAD_URL = "https://github.com/PowerShell/Win32-OpenSSH/releases/download/v8.1.0.0p1-Beta/$OPENSSH_ZIP"
$OPENSSH_INSTALL_DIR = Join-Path $PROGRAM_FILES $OPENSSH_NAME

function New-TemporaryDirectory {
	$TmpPath = [System.IO.Path]::GetTempPath()
    [string] $TmpDirName = [System.Guid]::NewGuid()
	$TmpDir = Join-Path $TmpPath $TmpDirName

	New-Item -ItemType Directory -Path $TmpDir
	return $TmpDir
}

$DownloadDir = New-TemporaryDirectory
$DownloadDir = $DownloadDir[-1]
Write-Output $DownloadDir

# Install OpenSSH
$OpenSshZip = Join-Path $DownloadDir "OpenSSH-Win64.zip"
Invoke-WebRequest $OPENSSH_DOWNLOAD_URL -OutFile $OpenSshZip
Expand-Archive -LiteralPath $OpenSshZip -DestinationPath $PROGRAM_FILES
powershell.exe -ExecutionPolicy Bypass -File (Join-Path $OPENSSH_INSTALL_DIR "install-sshd.ps1")

# Open firewall for SSHD
New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22

# Add public key used for starting instance to .ssh dir
New-Item -ItemType Directory -Path ~\.ssh
Invoke-RestMethod -uri http://169.254.169.254/latest/meta-data/public-keys/0/openssh-key > ~\.ssh\jmajkutewicz-windows.pub

# Change default shell used by OpenSSH to powershell
New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -PropertyType String -Force
New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShellCommandOption -Value "/c" -PropertyType String -Force

# Start SSHD
net start sshd
