FROM python:3.8-buster
WORKDIR "/root"

RUN set -x && \
    apt-get update && \
    apt-get install sshpass && \
    rm -rf /var/lib/apt/lists/*

COPY requirements.txt requirements.txt
RUN set -x && \
    pip3 install -r requirements.txt

COPY requirements.yml requirements.yml
RUN set -x && \
    ansible-galaxy install -r requirements.yml

ENV WORKING_DIR=/root/aws-ami-builder
RUN mkdir -p "$WORKING_DIR"
WORKDIR "$WORKING_DIR"

ENV ANSIBLE_CONFIG=ansible.cfg