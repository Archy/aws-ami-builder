#!/usr/bin/env bash
set -euxo pipefail

working_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

docker build -t ami-builder .

docker run -it --rm \
    --name ami-builder \
    -v "${working_dir}":/root/aws-ami-builder \
    -e TZ=Europe/Warsaw \
    -e AWS_ACCESS_KEY_ID \
    -e AWS_SECRET_ACCESS_KEY \
    ami-builder /bin/bash

# ansible-playbook create-ubuntu-ami.yml --key-file .ssh/jmajkutewicz-windows.pem
# ansible-playbook create-windows-ami.yml --key-file .ssh/jmajkutewicz-windows.pem
